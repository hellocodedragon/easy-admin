/**
 * 分页查询模型列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/workflow/definition/list',
        method: 'post',
        data: data
    })
}


