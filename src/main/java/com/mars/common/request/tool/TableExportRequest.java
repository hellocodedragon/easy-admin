package com.mars.common.request.tool;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 表导出DTO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class TableExportRequest {

    @ApiModelProperty(value = "ip")
    private String ip;

    @ApiModelProperty(value = "端口")
    private String port;

    @ApiModelProperty(value = "数据库")
    private String schema;

    @ApiModelProperty(value = "用户名")
    private String user;

    @ApiModelProperty(value = "密码")
    private String password;

    @NotNull
    @ApiModelProperty(value = "表名")
    private List<String> tableNameList;


}
