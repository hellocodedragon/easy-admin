package com.mars.common.request.tool;

import lombok.Data;

/**
 * Api参数
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class ApiParamRequest {

    private String name;

    private String comment;

    private String type;

    private String required;


}
