package com.mars.common.response;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 列表查询封装VO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class PageInfo<T> {

    private static final long serialVersionUID = -3862949514414749919L;

    @ApiModelProperty("数据总条数")
    private int count;

    @ApiModelProperty("分页数据")
    private List<T> list;

    public PageInfo(@NotNull List<T> list) {
        this.count = list.size();
        this.list = list;
    }

    public PageInfo(int total, @NotNull List<T> list) {
        this.count = total;
        this.list = list;
    }

    /**
     * 分页返回对象
     *
     * @param page page
     * @param <T>  <T>
     * @return <T>
     */
    public static <T> PageInfo<T> build(IPage<T> page) {
        PageInfo<T> rspData = new PageInfo<>();
        rspData.setList(page.getRecords());
        rspData.setCount((int)page.getTotal());
        return rspData;
    }

    public PageInfo() {
    }
}
