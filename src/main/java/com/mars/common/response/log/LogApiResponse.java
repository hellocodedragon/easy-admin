package com.mars.common.response.log;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 接口日志列表VO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class LogApiResponse {

    @ApiModelProperty(value = "ID")
    private Long id;

    @ApiModelProperty(value = "用户")
    private String realName;

    @ApiModelProperty(value = "接口地址")
    private String url;

    @ApiModelProperty(value = "请求参数")
    private String param;

    @ApiModelProperty(value = "耗时")
    private Integer time;

    @ApiModelProperty(value = "IP")
    private String ip;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;


}
