package com.mars.common.response.tool;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 数据库表列表VO
 *
 * @author 源码字节-程序员Mars
 */
@Data
@ApiModel(value = "数据库响应参数")
public class TableListResponse {

    @ApiModelProperty(value = "数据库名称")
    private String tableSchema;

    @ApiModelProperty(value = "表名")
    private String tableName;

    @ApiModelProperty(value = "备注")
    private String tableComment;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;


}
