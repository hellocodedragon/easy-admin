package com.mars.framework.reader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2023-11-08 15:55:21
 */
@Component
public class ClassMethodReader {


    /**
     * 读取某个类的方法
     *
     * @param cClass cClass
     * @return List<String>
     */
    public List<String> readClassMethods(Class<?> cClass) {
        Method[] declaredMethods = cClass.getDeclaredMethods();
        ArrayList<String> nameList = new ArrayList<>();
        for (Method declaredMethod : declaredMethods) {
            nameList.add(declaredMethod.getName());
        }
        return nameList;
    }
}
