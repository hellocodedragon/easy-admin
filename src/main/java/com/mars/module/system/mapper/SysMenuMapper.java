package com.mars.module.system.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.common.request.sys.SysMenuQueryRequest;
import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.system.entity.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 菜单
 *
 * @author 源码字节-程序员Mars
 */
public interface SysMenuMapper extends BasePlusMapper<SysMenu> {


    /**
     * 根据条件查询
     *
     * @param queryDto queryDto
     * @return List<SysMenu>
     */
    List<SysMenu> selectByCondition(SysMenuQueryRequest queryDto);

    /**
     * 根据userId查询菜单
     *
     * @param userId userId
     * @return List<SysMenu>
     */
    List<SysMenu> selectByUserId(Long userId);

    /**
     * 分页查询
     *
     * @param page     page
     * @param queryDto queryDto
     * @return IPage<SysMenu>
     */
    IPage<SysMenu> selectPageList(@Param("page") IPage<Object> page, @Param("queryDto") SysMenuQueryRequest queryDto);
}
