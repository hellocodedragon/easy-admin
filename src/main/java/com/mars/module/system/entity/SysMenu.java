package com.mars.module.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 菜单
 *
 * @author 源码字节-程序员Mars
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysMenu extends BaseEntity {
    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 名称
     */
    private String menuName;

    /**
     * 1.目录 2 菜单 3 按钮
     */
    private Integer menuType;
    /**
     * URL
     */
    private String url;

    /**
     * 父ID
     */
    private Long parentId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 权限标识
     */
    private String perms;

    /**
     * 图标
     */
    private String icon;

    /**
     * 备注
     */
    private String remark;

    /**
     * 基于哪个表生成的菜单
     */
    private String tableSource;


}
