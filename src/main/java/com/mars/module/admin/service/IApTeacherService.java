package com.mars.module.admin.service;

import com.mars.module.admin.entity.ApTeacher;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.ApTeacherRequest;

import java.util.List;

/**
 * 教师接口
 *
 * @author mars
 * @date 2024-03-23
 */
public interface IApTeacherService {
    /**
     * 新增
     *
     * @param param param
     * @return ApTeacher
     */
    ApTeacher add(ApTeacherRequest param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(ApTeacherRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return ApTeacher
     */
    ApTeacher getById(Long id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<ApTeacher>
     */
    PageInfo<ApTeacher> pageList(ApTeacherRequest param);


    /**
     * 查询所有数据
     *
     * @return List<ApTeacher>
     */
    List<ApTeacher> list(ApTeacherRequest param);
}
