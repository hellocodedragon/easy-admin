package com.mars.module.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.*;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.mars.module.system.entity.BaseEntity;

/**
 * 学生成绩对象 students
 *
 * @author mars
 * @date 2024-03-12
 */

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "学生成绩对象")
@Builder
@Accessors(chain = true)
@TableName("students")
public class Students extends BaseEntity {

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Integer id;
    /**
     * 用户ID
     */
    @Excel(name = "用户ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "用户ID")
    private Long userId;
    /**
     * 姓名
     */
    @Excel(name = "姓名")
    @ApiModelProperty(value = "姓名")
    private String name;
    /**
     * 年龄
     */
    @Excel(name = "年龄")
    @ApiModelProperty(value = "年龄")
    private Integer age;
    /**
     * 性别
     */
    @Excel(name = "性别")
    @ApiModelProperty(value = "性别")
    private Integer gender;
    /**
     * 数学
     */
    @Excel(name = "数学")
    @ApiModelProperty(value = "数学")
    private Integer math;
    /**
     * 英语
     */
    @Excel(name = "英语")
    @ApiModelProperty(value = "英语")
    private Integer english;
    /**
     * 语文
     */
    @Excel(name = "语文")
    @ApiModelProperty(value = "语文")
    private Integer chinese;
    /**
     * 审核状态
     */
    @Excel(name = "审核状态")
    @ApiModelProperty(value = "审核状态")
    private Integer status;
}
