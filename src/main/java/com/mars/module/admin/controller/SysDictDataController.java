package com.mars.module.admin.controller;


import java.util.Arrays;
import java.util.List;

import com.mars.common.enums.BusinessType;
import com.mars.common.response.PageInfo;
import com.mars.common.result.R;
import com.mars.module.admin.entity.SysDictData;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.ISysDictDataService;
import com.mars.module.admin.request.SysDictDataRequest;

/**
 * 字典数据控制层
 *
 * @author mars
 * @date 2023-11-18
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "字典数据接口管理" , tags = "字典数据接口管理")
@RequestMapping("/admin/sysDictData")
public class SysDictDataController {

    private final ISysDictDataService iSysDictDataService;

    /**
     * 分页查询字典数据列表
     */
    @ApiOperation(value = "分页查询字典数据列表")
    @PostMapping("/pageList")
    public R<PageInfo<SysDictData>> list(@RequestBody SysDictDataRequest sysDictData) {
        return R.success(iSysDictDataService.pageList(sysDictData));
    }

    /**
     * 获取字典数据详细信息
     */
    @ApiOperation(value = "获取字典数据详细信息")
    @GetMapping(value = "/query/{id}")
    public R<SysDictData> getInfo(@PathVariable("id") Long id) {
        return R.success(iSysDictDataService.getById(id));
    }

    /**
     * 通过类型获取字典列表
     */
    @ApiOperation(value = "通过类型获取字典列表")
    @GetMapping(value = "/getDictData/{type}")
    public R<List<SysDictData>> getDictData(@PathVariable("type") String type) {
        return R.success(iSysDictDataService.getDictData(type));
    }

    /**
     * 新增字典数据
     */
    @Log(title = "新增字典数据" , businessType = BusinessType.INSERT)
    @ApiOperation(value = "新增字典数据")
    @PostMapping("/add")
    public R<Void> add(@RequestBody SysDictDataRequest sysDictData) {
        iSysDictDataService.add(sysDictData);
        return R.success();
    }

    /**
     * 修改字典数据
     */
    @Log(title = "修改字典数据" , businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改字典数据")
    @PostMapping("/update")
    public R<Void> edit(@RequestBody SysDictDataRequest sysDictData) {
        iSysDictDataService.update(sysDictData);
        return R.success();
    }

    /**
     * 删除字典数据
     */
    @Log(title = "删除字典数据" , businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除字典数据")
    @PostMapping("/delete/{ids}")
    public R<Void> remove(@PathVariable Long[] ids) {
        iSysDictDataService.deleteBatch(Arrays.asList(ids));
        return R.success();
    }
}
