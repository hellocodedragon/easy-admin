package com.mars.module.tool.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.mars.module.system.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 业务表控制器
 *
 * @author mars
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GenControllerMethods extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 表id
     */
    private Long tableId;


    /**
     * 是否生成添加
     */
    private Integer isAdd;
    /**
     * 是否生成删除
     */
    private Integer isDelete;
    /**
     * 是否生成更新
     */
    private Integer isUpdate;
    /**
     * 是否生成查询
     */
    private Integer isList;
    /**
     * 是否导入
     */
    private Integer isExport;
    /**
     * 是否导出
     */
    private Integer isImport;


}
