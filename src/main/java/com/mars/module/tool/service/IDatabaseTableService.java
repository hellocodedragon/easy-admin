package com.mars.module.tool.service;


import com.mars.common.request.tool.TableQueryRequest;
import com.mars.common.response.PageInfo;
import com.mars.common.response.tool.TableListResponse;

/**
 * 数据库表接口
 *
 * @author mars
 */
public interface IDatabaseTableService {

    /**
     * 分页查询表
     *
     * @param request 请求参数
     * @return PageInfo<TableListResponse>
     */
    PageInfo<TableListResponse> list(TableQueryRequest request);
}
